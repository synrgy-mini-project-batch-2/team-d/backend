'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Recipes extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Users, {
                foreignKey: 'userId',
                foreignKeyConstraint: true,
                onDelete: 'SET NULL'
            })
        }
    };
    Recipes.init({
        title: DataTypes.STRING,
        userId: DataTypes.INTEGER,
        ingredients: DataTypes.TEXT,
        directions: DataTypes.TEXT,
        portions: DataTypes.STRING,
        duration: DataTypes.STRING,
        imageUrl: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'Recipes',
    });
    return Recipes;
};