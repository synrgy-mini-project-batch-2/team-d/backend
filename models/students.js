'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Students extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Users, {
                foreignKey: 'userId',
                foreignKeyConstraint: true,
                onDelete: 'SET NULL'
            });
            this.belongsToMany(models.Courses, {
                through: 'StudentCourses'
            })
            this.hasMany(models.PurchaseHistorys, {
                foreignKey: 'studentId',
                foreignKeyConstraint: true,
                onDelete: 'CASCADE'
            })
            this.hasMany(models.Reviews, {
                foreignKey: 'studentId',
                foreignKeyConstraint: true,
                onDelete: 'SET NULL'
            })
        }
    };
    Students.init({
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        imageUrl: DataTypes.STRING,
        userId: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'Students',
    });
    return Students;
};