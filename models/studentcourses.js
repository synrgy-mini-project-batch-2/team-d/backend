'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentCourses extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  StudentCourses.init({
    StudentId: DataTypes.INTEGER,
    CourseId: DataTypes.INTEGER,
    isOwned: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'StudentCourses',
  });
  return StudentCourses;
};