'use strict';
const { text } = require('body-parser');
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
class Courses extends Model {
  /**
  * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsToMany(models.Students, {
                through: 'StudentCourses'
            });
            this.belongsTo(models.Users, {
                foreignKey: 'userId',
                foreignKeyConstraint: true,
                onDelete: 'CASCADE'
            })
            this.belongsTo(models.Teachers, {
                foreignKey: 'teacherId',
                foreignKeyConstraint: true,
                onDelete: 'CASCADE'
            })
            this.hasOne(models.PurchaseHistorys, {
                foreignKey: 'courseId',
                foreignKeyConstraint: true,
                onDelete: 'CASCADE'
            })
            this.hasMany(models.Reviews, {
              foreignKey: 'courseId',
              foreignKeyConstraint: true,
              onDelete: 'SET NULL'
            })
        }
    };
    Courses.init({
        title: DataTypes.STRING,
        videoUrl: DataTypes.STRING,
        price: DataTypes.BIGINT,
        isPaid: DataTypes.BOOLEAN,
        userId: DataTypes.INTEGER,
        teacherId: DataTypes.INTEGER,
        region: DataTypes.STRING,
        ingredients: DataTypes.TEXT,
        directions: DataTypes.TEXT,
        level: DataTypes.STRING,
        duration: DataTypes.STRING,
        previewUrl: DataTypes.STRING,
        description: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'Courses',
    });
    return Courses;
  }