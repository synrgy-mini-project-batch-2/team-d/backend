'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teachers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, {
        foreignKey: 'userId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      })
      this.hasMany(models.Courses, {
        foreignKey: 'teacherId',
        foreignKeyConstraint: true,
        onDelete: 'CASCADE'
      })
    }
  };
  Teachers.init({
    name: DataTypes.STRING,
    imageUrl: DataTypes.STRING,
    region: DataTypes.STRING,
    biography: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Teachers',
  });
  return Teachers;
};