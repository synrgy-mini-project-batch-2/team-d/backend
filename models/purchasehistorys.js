'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class PurchaseHistorys extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Students, {
                foreignKey: 'studentId',
                foreignKeyConstraint: true,
                onDelete: 'CASCADE'
            })
            this.belongsTo(models.Courses, {
                foreignKey: 'courseId',
                foreignKeyConstraint: true,
                onDelete: 'CASCADE'
            })
        }
    };
    PurchaseHistorys.init({
        studentId: DataTypes.INTEGER,
        courseId: DataTypes.INTEGER,
        purchaseId: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'PurchaseHistorys',
    });
    return PurchaseHistorys;
};