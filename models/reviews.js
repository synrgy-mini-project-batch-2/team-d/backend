'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reviews extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Students, {
        foreignKey: 'studentId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      });
      this.belongsTo(models.Courses, {
        foreignKey: 'courseId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      });
    }
  };
  Reviews.init({
    content: DataTypes.TEXT,
    score: DataTypes.FLOAT,
    studentId: DataTypes.INTEGER,
    courseId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Reviews',
  });
  return Reviews;
};