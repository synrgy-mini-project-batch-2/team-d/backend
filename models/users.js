'use strict';
const {
  Model
} = require('sequelize');


module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Roles, {
        foreignKey: 'roleId',
        onDelete: 'SET NULL'
      });
      this.hasMany(models.Articles, {
        foreignKey: 'userId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      });
      this.hasMany(models.Recipes, {
        foreignKey: 'userId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      });
      this.hasMany(models.Teachers, {
        foreignKey: 'userId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      })
      this.hasMany(models.Courses, {
        foreignKey: 'userId',
        foreignKeyConstraint: true,
        onDelete: 'CASCADE'
      })
      this.hasOne(models.Students, {
        foreignKey: 'userId',
        foreignKeyConstraint: true,
        onDelete: 'SET NULL'
      })
    }
  };
  Users.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    roleId: DataTypes.INTEGER,
    isVerified: DataTypes.BOOLEAN,
    verifyToken: DataTypes.STRING,
    resetToken: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};