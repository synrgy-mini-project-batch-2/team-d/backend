const { Articles } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { validationError } = require('../helpers/validationErrorHelper')
const bucket = require('../config/firebase');

exports.getArticles = async(req, res, next) => {
    try {
        const fetchedArticles =
            await Articles.findAll({ attributes: [ 'id', 'title', 'imageUrl', 'createdAt' ] });

        if (!fetchedArticles) {
            return next(createErrHandler(404, 'No articles found'));
        }

        return res.status(200).json({
            success: true,
            articles: fetchedArticles
        })
    } catch (error) {
        return next(asyncErrHandler(error));
    }
}

exports.getArticleById = async(req, res, next) => {
    if (req) {
        const id = req.params.id;

        try {
            const fetchedArticle = await Articles.findOne({ where: { id } });

            if (!fetchedArticle) {
                return next(createErrHandler(404, 'Article not found'));
            }

            return res.status(200).json({
                success: true,
                article: fetchedArticle
            })
        } catch (err) {
            return next(asyncErrHandler(err));
        }
    }
}

exports.postArticle = async(req, res, next) => {
    if (validationError(req)) {
        return next(validationError(req));
    }

    if (!req.file) {
        const error = createErrHandler(422, 'No file picked');
        return next(error);
    }

    const { title, content } = req.body

    const image = req.file.originalname;
    let publicUrl;
    const blob = bucket.file(image)

    const blobStream = blob.createWriteStream({
        metadata: {
            contentType: req.file.mimetype
        }
    })

    blobStream.on('error', err => {
        return next(err)
    })

    blobStream.on('finish', async() => {
        publicUrl = `https://firebasestorage.googleapis.com/v0/b/${
                bucket.name
            }/o/${encodeURI(blob.name)}?alt=media`;

        try {
            await req.user.createArticle({
                    title,
                    content,
                    imageUrl: publicUrl,
                })

            return res.status(202).json({
                success: true,
                message: 'Article created successfully'
            })
        } catch (err) {
            return next(asyncErrHandler(err));
        }
    })

    blobStream.end(req.file.buffer)
}


exports.putArticle = async(req, res, next) => {
    if (validationError(req)) {
        return next(validationError(req))
    }
    const id = req.params.id;
    const { title, content, imageUrl } = req.body
    try {
        const fetchedArticles = await Articles.findOne({ where: { id } })

        if (!fetchedArticles) {
            return next(createErrHandler(404, 'Article not found'))
        }

        await fetchedArticles.update({
            title,
            content,
            imageUrl
        })

        return res.status(200).json({
            success: true,
            message: `Article with id ${id} successfully updated`
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}

exports.deleteArticleById = async(req, res, next) => {
    const id = req.params.id;

    try {
        await Articles.destroy({ where: { id } });

        return res.status(200).json({
            success: true,
            message: `Article with id ${id} successfully deleted`
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}
