const { Students, Users } = require('../models')
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { validationError } = require('../helpers/validationErrorHelper');
const bucket = require('../config/firebase');
const { hashPassword, comparePass } = require('../helpers/bcryptHelper');

exports.getAllStudents = async (req, res, next) => {
  try {
    const fetchedStudents = await Students.findAll();

    return res.status(200).json({
      success: true,
      message: 'Successfully fetched all students',
      students: fetchedStudents
    })
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.getStudentById = async (req, res, next) => {
  const id = req.params.id;
  if(!id){
    return next(createErrHandler(400, 'No ID supplied'))
  }
  try {
    const fetchedStudent = await Students.findOne({where: {id}, include: {
      model: Users,
      attributes: [ 'email' ]
    }});
    if(!fetchedStudent){
      const error = createErrHandler(404, 'Student not found');
      return next(error);
    }

    return res.status(200).json({
      success: true,
      message: `Student with id ${id} found`,
      student: fetchedStudent
    });
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.getStudentInfo = async (req, res, next) => {
  const id = req.user.id;
  if(!id){
    const error = createErrHandler(400, 'No ID Supplied')
    return next(error)
  }
  try {
    const fetchedStudent = await req.user.getStudent();

    if(!fetchedStudent){
      const error = createErrHandler(404, 'Student not found');
      return next(error);
    }
    return res.status(200).json({
      success: true,
      message: 'Successfully fetched student info',
      student: fetchedStudent
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
  
}

exports.putStudentById = async (req, res, next) => {
  if(validationError(req)){
    if(req.file){
      await bucket.file(req.file.originalname).delete();
    }
    return next(validationError(req));
  }
  const id = req.params.id;
  if(!id){
    const error = createErrHandler(400, 'No ID supplied');
    return next(error);
  }

  const { firstName, lastName, address } = req.body;
  let publicUrl;

  try {

    const fetchedUser = await Students.findOne({where: { id }})
    if(req.file){
      const image = req.file.originalname;
      if(req.user.imageUrl){
        const index = fetchedUser.imageUrl.lastIndexOf('/');
        const alt = fetchedUser.imageUrl.lastIndexOf('?')
        let imageName = fetchedUser.imageUrl.substring(index + 1, alt);
        await bucket.file(imageName).delete();
      }

      const blob = bucket.file(image);

      const blobStream = blob.createWriteStream({
        metadata: {
          contentType: req.file.mimetype
        }
      });

      blobStream.on('error', (err) => {
        return next(err)
      })

      blobStream.on('finish', async () => {
        publicUrl = `https://firebasestorage.googleapis.com/v0/b/${
          bucket.name
        }/o/${encodeURI(blob.name)}?alt=media`;

        

        await fetchedUser.update({
          firstName,
          lastName,
          address,
          imageUrl: publicUrl
        })

        return res.status(200).json({
          success: true,
          message: `Profile successfully updated`
        })
      })
      blobStream.end(req.file.buffer)
    } else {
      await fetchedUser.update({
        firstName,
        lastName,
        address
      })
  
      return res.status(200).json({
        success: true,
        message: `Profile successfully updated`
      })
    }
    
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.postStudentPassword = async (req, res, next) => {
  const { password } = req.body;

  const compare = comparePass(password, req.user.password);
  if(!compare){
    const error = createErrHandler('400', 'Password doesn\'t match');
    return next(error);
  }
  return res.status(200).json({
    success: true,
    message: 'Password matches!'
  })
}

exports.putStudentPassword = async (req, res, next) => {
  const { newPassword } = req.body;
  try {
    const hashedPassword = hashPassword(newPassword);

    await req.user.update({
      password: hashedPassword
    })
    return res.status(200).json({
      success: true,
      message: 'Password updated successfully'
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}

exports.deleteStudentById = async (req, res, next) => {
  const id = req.params.id;
  if(!id){
    const error = createErrHandler(400, 'No ID supplied');
    return next(error)
  }

  try {
    const fetchedStudent = await Students.findOne({where: {id}});

    if(!fetchedStudent){
      const error = createErrHandler(404, 'Student not found');
      return next(error)
    }

    const index = fetchedStudent.imageUrl.lastIndexOf('/');
    const alt = fetchedStudent.imageUrl.lastIndexOf('?');
    const imageName = fetchedStudent.imageUrl.substring(index + 1, alt);

    await bucket.file(imageName).delete();

    await fetchedStudent.destroy();

    return res.status(200).json({
      success: true,
      message: `Student with id ${id} successfully deleted`
    })
  } catch (err) {
    return asyncErrHandler(err)
  }
}