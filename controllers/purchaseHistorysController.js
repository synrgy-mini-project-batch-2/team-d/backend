const { PurchaseHistorys, Students, Courses } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { validationError } = require('../helpers/validationErrorHelper');

exports.getAllPurchaseHistorys = async(req, res, next) => {
    try {
        const fetchedStudent = await req.user.getStudent();
        const fetchedHistories = await fetchedStudent.getPurchaseHistorys({ include: [ Courses ] });

        return res.status(200).json({
            success: true,
            message: 'Successfully fetched all purchase histories',
            histories: fetchedHistories
        })
    } catch (error) {
        return next(asyncErrHandler(error));
    }

}

exports.getPurchaseHistoryById = async(req, res, next) => {
    const id = req.params.id;
    if (req) {
        const fetchedStudent = await req.user.getStudent();

        if(!fetchedStudent){
            const error = createErrHandler(404, 'Student not found');
            return next(error)
        }
        const fetchedHistory = await fetchedStudent.getPurchaseHistorys({ where: { courseId: id }, include: [ Courses ] });

        if(!fetchedHistory.length){
            const error = createErrHandler(404, 'History not found');
            return next(error)
        }

        return res.status(200).json({
            success: true,
            message: `Successfully fetched history with id ${id}`,
            history: fetchedHistory
        })
    }
}
exports.postPurchaseHistorys = async(req, res, next) => {
    if (validationError(req)) {
        return next(validationError(req))
    }
    const { Students, Courses } = req.body
    try {
        await req.user.createRecipe({
            Students,
            Courses
        })

        return res.status(202).json({
            success: true,
            message: `Purchase History created successfully`
        })
    } catch (err) {
        return next(asyncErrHandler(err));
    }
}

exports.putPurchaseHistoryById = async(req, res, next) => {
    if (validationError(req)) {
        return next(validationError(req))
    }
    const id = req.params.id;
    const { studentId, courseId } = req.body
    try {
        const fetchedPurchaseHistorys = await PurchaseHistorys.findOne({ where: { id } })

        if (!fetchedPurchaseHistorys) {
            return next(createErrHandler(404, 'Recipe not found'))
        }
        await fetchedPurchaseHistorys.update({
            studentId,
            courseId
        })

        return res.status(200).json({
            success: true,
            message: `Purchase History with id ${id} successfully updated`
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}

exports.deletePurchaseHistoryById = async(req, res, next) => {
    const id = req.params.id;

    try {
        await PurchaseHistorys.destroy({ where: { id } });

        return res.status(200).json({
            success: true,
            message: `Purchase History with id ${id} successfully deleted`
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}