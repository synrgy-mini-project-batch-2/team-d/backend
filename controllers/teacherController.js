const { Teachers } = require('../models');

const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { validationError } = require('../helpers/validationErrorHelper');

exports.getAllTeachers = async (req, res, next) => {
  try {
    const fetchedTeachers = await Teachers.findAll();

    return res.status(200).json({
      success: true,
      message: 'Successfully fetched Teachers',
      teachers: fetchedTeachers
    });
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.getTeacherById = async (req, res, next) => {
  const id = req.params.id;

  try {
    const fetchedTeacher = await Teachers.findOne({ where: { id } });

    if(!fetchedTeacher){
      const error = createErrHandler(404, 'Teacher not found');
      return next(error)
    }

    return res.status(200).json({
      success: true,
      message: `Teacher with id ${id} successfully fetched`,
      teacher: fetchedTeacher
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}

exports.postTeacher = async (req, res, next) => {
  if(validationError(req)){
    return next(validationError(req))
  }

  const { name, imageUrl, region, biography } = req.body;

  try {
    await req.user.createTeacher({
      name,
      imageUrl,
      region,
      biography
    });
  
    return res.status(202).json({
      success: true,
      message: 'Teacher successfully created',
    })
  } catch (err) {
    return next(asyncErrHandler(err));
  }
  
}

exports.putTeacherById = async (req, res, next) => {
  if(validationError(req)){
    return next(validationError(req));
  }

  const id = req.params.id;
  const { name, imageUrl, region, biography } = req.body;

  try {
    const fetchedTeacher = await Teachers.findOne({ where: { id }})

    if(!fetchedTeacher){
      const error = createErrHandler(404, 'Teacher not found');
      return next(error);
    }

    await fetchedTeacher.update({
      name,
      imageUrl,
      region,
      biography
    })
    return res.status(200).json({
      success: true,
      message: `Successfully updated Teacher with id ${id}`
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}

exports.deleteTeacherById = async (req, res, next) => {
  const id = req.params.id;

  try {
    const fetchedTeacher = await Teachers.findOne({ where: { id } });

    if(!fetchedTeacher){
      const error = createErrHandler(404, 'Teacher not found');
      return next(error)
    }

    await fetchedTeacher.destroy();

    return res.status(200).json({
      success: true,
      message: `Successfully deleted Teacher with id ${id}`
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}