const { Courses, Teachers, Reviews, Students } = require("../models")
const { validationError } = require('../helpers/validationErrorHelper')
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { Op } = require("sequelize");
const bucket = require('../config/firebase');

//CRUD Courses Class
exports.getCourses = async (req, res, next) => {
    try {
        const fetchedCourses = await Courses.findAll({ include: Teachers, attributes: [ 'id', 'title', 'previewUrl', 'price', 'region', 'level', 'duration' ] });

        if(!fetchedCourses){
            return next(createErrHandler(404, 'No Courses found'));
        }
        return res.status(200).json({
            success: true,
            courses: fetchedCourses
        })
    } catch (error){
        return next(asyncErrHandler(error));
    }
} 

exports.getCoursesById = async (req, res, next) => {
    if(req){
        const id = req.params.id;
        const fetchedCourse = await Courses.findOne({ where: { id }, include: [ Teachers ] });
    try {
        if(req.user){
            const fetchedStudent = await req.user.getStudent();
            if(!fetchedStudent){
                const error = createErrHandler(404, 'No Student found');
                return next(error)
            }

            const fetchedOwnedCourses = await fetchedStudent.getCourses({ include: [ Teachers, Reviews ] });

            const fetchedCourse = await Courses.findOne({ where: { id }, include: [ Teachers ] });


            if(!fetchedCourse){
                const error = createErrHandler(404, 'Course not found');
                return next(error);
            }

            const filtered = fetchedOwnedCourses.filter(course => {
                return course.title === fetchedCourse.title
            }
            )
            

            if(filtered.length > 0){
                return res.status(200).json({
                    status: true,
                    message: `Successfully fetched course with id ${id}`,
                    course: filtered[0],
                    isOwned: true
                })
            } else {
                return res.status(200).json({
                    status: true,
                    message: `Successfully fetched course with id${id}`,
                    course: fetchedCourse,
                    isOwned: false
                })
            }
        } else {
            return res.status(200).json({
                success: true,
                message: `Successfully fetched course with id ${id}`,
                course: fetchedCourse,
                isOwned: false
            })
        }
    } catch (error) {
        return next(asyncErrHandler(error));
        
    }
}}

exports.getCoursesByRegion = async (req, res, next) => {
    const region = req.query.region;
    if(!region){
        const error = createErrHandler(400, 'No Query String supplied');
        return next(error);
    }
    const fetchedCourses = await Courses.findAll({ where: { region } });

    if(!fetchedCourses){
        const error = createErrHandler(404, 'Courses with such region not found');
        return next(error);
    }

    return res.status(200).json({
        success: true,
        message: 'Successfully fetched courses',
        courses: fetchedCourses
    })
}

exports.getCoursesBySearch = async (req, res, next) => {
    const search = req.query.search;
    if(search){
        const fetchedCourses = await Courses.findAll({
            where: {
                title: {
                    [Op.like]: `${searchValue}%`
                }
            }
        })

        if(!fetchedCourses){
            const error = createErrHandler(404, 'Courses not found');
            return next(error);
        }

        return res.status(200).json({
            success: true,
            message: 'Successfully fetched courses',
            courses: fetchedCourses
        })
    }
}

exports.createCourses = async (req, res, next) => {
    if(validationError(req)){
        return next(validationError(req));
    }

    if(req.files.length !== 2){
        const error = createErrHandler(422, 'No file picked');
        return next(error);
    }

    const { title, price, isPaid, teacherId, region, ingredients, directions, level, duration, description } = req.body

    let videoUrl;
    let imgUrl;

    let videoMime;
    let imgMime;

    let videoBuffer;
    let imgBuffer;
    
    req.files.forEach(file => {
        if(file.mimetype === 'video/mp4' ||
        file.mimetype === 'video/avi' ||
        file.mimetype === 'video/mov' ){
            videoUrl = file.originalname;
            videoMime = file.mimetype;
            videoBuffer = file.buffer;
        } else {
            imgUrl = file.originalname;
            imgMime = file.mimetype;
            imgBuffer = file.buffer;
        }
    })

    let videoPublicUrl;
    let imagePublicUrl;
    
    const videoBlob = bucket.file(videoUrl);
    const imgBlob = bucket.file(imgUrl)

    const videoBlobStream = videoBlob.createWriteStream({
        metadata: {
            contentType: videoMime
        }
    })

    videoBlobStream.on('error', err => {
        return next(err)
    })

    videoBlobStream.on('finish', async () => {
        videoPublicUrl = `https://firebasestorage.googleapis.com/v0/b/${
            bucket.name
            }/o/${encodeURI(videoBlob.name)}?alt=media`;

            const imageBlobStream = imgBlob.createWriteStream({
                metadata: {
                    contentType: imgMime
                }
            })
        
            imageBlobStream.on('error', err => {
                return next(err)
            })
        
            imageBlobStream.on('finish', async () => {
                imagePublicUrl = `https://firebasestorage.googleapis.com/v0/b/${
                    bucket.name
                    }/o/${encodeURI(imgBlob.name)}?alt=media`;

                    try {
                        await req.user.createCourse({
                            title,
                            videoUrl: videoPublicUrl,
                            price: +price,
                            isPaid,
                            teacherId,
                            region,
                            ingredients,
                            directions,
                            level,
                            duration,
                            previewUrl: imagePublicUrl,
                            description
                        })
                        return res.status(202).json({
                            success: true,
                            message: 'Courses created successfully'
                        })
                    } catch (error) {
                        return next(asyncErrHandler(error));
                    }
        })
        imageBlobStream.end(imgBuffer);
})
    videoBlobStream.end(videoBuffer);
}        

exports.putCourses = async (req, res, next) => {
    if(validationError(req)){
        if(req.files){
            await bucket.file(req.files[0].originalname).delete();
            await bucket.file(req.files[1].originalname).delete();
        }
        return next(validationError(req))
    }
    const id = req.params.id;
    const { title, vidUrl, price, isPaid, teacherId, region, ingredients, directions, level, duration, imageUrl, description } = req.body
    
    let videoUrl;
    let imgUrl;

    if(req.files.length){
        req.files.forEach(file => {
            if(file.mimetype === 'video/mp4' ||
            file.mimetype === 'video/avi' ||
            file.mimetype === 'video/mov' ){
                videoUrl = file.originalname;
            } else {
                imgUrl = file.originalname;
            }
        })
    let videoPublicUrl;
    let imagePublicUrl;
    
    const videoBlob = bucket.file(videoUrl);
    const imgBlob = bucket.file(imgUrl)

    const videoBlobStream = videoBlob.createWriteStream({
        metadata: {
            contentType: req.files[0].mimetype
        }
    })

    videoBlobStream.on('error', err => {
        return next(err)
    })

    videoBlobStream.on('finish', async () => {
        videoPublicUrl = `https://firebasestorage.googleapis.com/v0/b/${
            bucket.name
            }/o/${encodeURI(videoBlob.name)}?alt=media`;

            const imageBlobStream = imgBlob.createWriteStream({
                metadata: {
                    contentType: req.files[1].mimetype
                }
            })
        
            imageBlobStream.on('error', err => {
                return next(err)
            })
        
            imageBlobStream.on('finish', async () => {
                imagePublicUrl = `https://firebasestorage.googleapis.com/v0/b/${
                    bucket.name
                    }/o/${encodeURI(imgBlob.name)}?alt=media`;

                    try {
                        const fetchedCourse = await Courses.findOne({ where: { id } });
                        if(!fetchedCourse){
                            const error = createErrHandler(404, 'Course not found');
                            return next(error);
                        }
                        await fetchedCourse.update({
                            title,
                            videoUrl: videoPublicUrl,
                            price: +price,
                            isPaid,
                            teacherId,
                            region,
                            ingredients,
                            directions,
                            level,
                            duration,
                            previewUrl: imagePublicUrl
                        })
                        return res.status(202).json({
                            success: true,
                            message: 'Courses created successfully'
                        })
                    } catch (error) {
                        return next(asyncErrHandler(error));
                    }
        })
        imageBlobStream.end(req.files[1].buffer);
})
    videoBlobStream.end(req.files[0].buffer);
} else {
    const fetchedCourse = await Courses.findOne({ where: { id } });
        if(!fetchedCourse){
            const error = createErrHandler(404, 'Course not found');
                return next(error);
        }

        await fetchedCourse.update({
            title,
            videoUrl: vidUrl,
            price: +price,
            isPaid,
            teacherId,
            region,
            ingredients,
            directions,
            level,
            duration,
            previewUrl: imageUrl
        });
        return res.status(200).json({
            success: true,
            message: `Course with id ${id} updated successfully`
        })
}
}

exports.deleteCoursesById = async (req, res, next) => {
    const id = req.params.id;
    try {
        await Courses.destroy({where : {id}});
        return res.status(200).json({
            status: true,
            message: 'Courses with ${id} successfully deleted'
        })
    } catch (error) {
        return next(asyncErrHandler(error))
        
    }
}

exports.getOwnedCourses = async (req, res, next) => {
    try {
        const fetchedStudent = await req.user.getStudent();

        if(!fetchedStudent){
            const error = createErrHandler(404, 'Student not found');
            return next(error);
        }
        const fetchedCourses = await fetchedStudent.getCourses( {include: [ Teachers ] });
        return res.status(200).json({
            success: true,
            message: 'Courses fetched successfully',
            courses: fetchedCourses
        })
    } catch (error) {
        return next(asyncErrHandler(error));
    }
}

exports.getOwnedCourseById = async (req, res, next) => {
    const id = req.params.id;

    try {
        const fetchedStudent = await req.user.getStudent();
        if(!fetchedStudent){
            const error = createErrHandler(404, 'Student not found');
            return next(error);
        }
        const fetchedCourse = await fetchedStudent.getCourses({ where: { id }, include: [ Teachers, {
            model: Reviews,
            include: [ Students ]
        } ] } )

        if(!fetchedCourse.length){
            const error = createErrHandler(404, 'No courses found');
            return next(error);
        }

        return res.status(200).json({
            success: true,
            message: `Successfully fetched owned course with id ${id}`,
            course: fetchedCourse[0]
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}