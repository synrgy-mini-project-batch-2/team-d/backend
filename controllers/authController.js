const { comparePass, hashPassword } = require('../helpers/bcryptHelper');
const { validationError } = require('../helpers/validationErrorHelper');
const { Users, Roles, Students } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { generateAccessToken, generateRefreshToken, generateVerifyToken, generateResetToken, verifyActivationToken, verifyResetToken } = require('../helpers/jwtHelper');
const sendEmail = require('../helpers/nodemailerHelper');

exports.loginHandler = async (req, res, next) => {
  if(validationError(req)){
    return next(validationError(req));
  }
  const { email } = req.body;

  try {
    const fetchedUser = await Users.findOne({where :{ email }})

    const role = await fetchedUser.getRole();

    const token = generateAccessToken({ userId: fetchedUser.id, role })

    return res.status(200).json({
      success: true,
      message: 'Successfully authenticated',
      credentials: {
        email: fetchedUser.email,
        role: role.rolename,
        token
      }
    })
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.registerHandler = async (req, res, next) => {
  if(validationError(req)){
    return next(validationError(req))
  }
  const { firstName, lastName, email, password } = req.body;

  const hashedPassword = hashPassword(password);

  try {
    const verifyToken = generateVerifyToken({ user: email });

    const link = `https://mini-project-frontend.herokuapp.com/auth/verify-email/${verifyToken}`

    const createdStudent = await Students.create({
      firstName,
      lastName,
      User: {
        email,
        password: hashedPassword,
        roleId: 5,
        verifyToken
      }
    }, {
      include: [ Users ]
    })

    const token = generateAccessToken({ userId: createdStudent.User.id, role: 'default' });

    sendEmail(createdStudent.User.email, "Verify Your Email", { name: createdStudent.firstName, link }, './templates/requestVerifyEmail.hbs');

    return res.status(200).json({
      success: true,
      message: 'Student registered successfully. Please check your email to verify.',
      token
    });
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.verifyEmail = async (req, res, next) => {
  const verifyToken = req.params.token;

  if(verifyToken){
    verifyActivationToken(verifyToken);

    try {
      const fetchedUser = await Users.findOne({ where: { verifyToken } });

      if(!fetchedUser){
        const error = createErrHandler(404, 'Couldn\'t find user');
        return next(error)
      }

      await fetchedUser.update({
        isVerified: true,
        verifyToken: null
      })

      return res.status(200).json({
        success: true,
        message: 'Successfully verified email'
      })
    } catch (err) {
      return next(asyncErrHandler(err));
    }
  }
}

exports.forgotPassword = async (req, res, next) => {
  const { email } = req.body;

  try {
    const fetchedUser = await Users.findOne({ where: { email }, include: {
      model: Students,
      attributes: [ 'firstName' ]
    } });

    if(!fetchedUser){
      const error = createErrHandler(404, 'User not found');
      return next(error)
    }

    const resetToken = generateResetToken({ user: fetchedUser.email });

    const link =`https:://mini-project-frontend.herokuapp.com/forget-password/new-password/${resetToken}`

    sendEmail(fetchedUser.email, "Password Reset Request", { name: fetchedUser.Student.firstName, link }, "./templates/requestResetPassword.hbs");

    await fetchedUser.update({
      resetToken
    })

    return res.status(200).json({
      success: true,
      message: 'Reset password link sent to email.'
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}

exports.putPassword = async (req, res, next) => {
  const resetToken = req.params.token;
  const { newPassword } = req.body;

  if(resetToken){
    verifyResetToken(resetToken);
  }

  try{
    const fetchedUser = await Users.findOne({ where: { resetToken } });

    if(!fetchedUser){
      const error = createErrHandler(404, 'Couldn\'t find user');
      return next(error)
    }

    const hashedPassword = hashPassword(newPassword);

    await fetchedUser.update({
      password: hashedPassword,
      resetToken: null
    })

    return res.status(200).json({
      success: true,
      message: 'Successfully reset password'
    })
  } catch (err){
    return next(asyncErrHandler(err))
  }
}