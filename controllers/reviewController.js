const { Reviews } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { validationError } = require('../helpers/validationErrorHelper')

exports.getReviews = async(req, res, next) => {
    try {
        const fetchedReview = await Reviews.findAll();
          if(!fetchedReview){
            return next(createErrHandler(404, 'No review found'));
          }
          return res.status(200).json({
            success: true,
            review: fetchedReview
          })
    } catch (error) {
        return next(asyncErrHandler(error));
    }
}

exports.getReviewById = async (req, res, next) =>{
    if(req){
      const id = req.params.id;
  
      try {
        const fetchedReview = await Reviews.findOne({ where: {id} });
  
        if(!fetchedReview){
          return next(createErrHandler(404, 'Review not found'));
        }
  
        return res.status(200).json({
          success: true,
          message: `Review with id ${id} fetched successfully`,
          review: fetchedReview
        })
      } catch (err) {
        return next(asyncErrHandler(err));
      }
    }
  }

exports.postReview = async(req, res, next) => {
  if(validationError(req)){
      return next(validationError(req))
  }

  const { content, score, courseId } = req.body
      
  try {
    const fetchedStudent = await req.user.getStudent();

    if(!fetchedStudent){
      const error = createErrHandler(404, 'Student not found');
      return next(error)
    }

    await fetchedStudent.createReview({
      content,
      score,
      courseId
    })

    return res.status(202).json({
      success: true,
      message: 'Successfully created review'
    })
  } catch (err) {
    return next(asyncErrHandler(err));
  }
}

exports.putReviewById = async(req, res, next) => {
  if(validationError(req)){
      return next(validationError(req))
  }
  const { content, score } = req.body
  const id = req.params.id;
  try {
    const fetchedStudent = await req.user.getStudent();

    if(!fetchedStudent){
      const error = createErrHandler(404, 'Student not found');
      return next(error);
    }

    const fetchedReview = await fetchedStudent.getReviews({ where: { id } })

    if(!fetchedReview.length){
      const error = createErrHandler(404, 'Review not found');
      return next(error)
    }

    await fetchedReview[0].update({
      content,
      score
    })

    return res.status(202).json({
      success: true,
      message: 'Review successfully updated'
    })
  } catch (err) {
      return next(asyncErrHandler(err))
  }
}

exports.deleteReviewById = async(req, res, next) => {
  const id = req.params.id;

  try {
      await Reviews.destroy({ where: { id } });

      return res.status(200).json({
          success: true,
          message: `Review with id ${id} successfully deleted`
      })
  } catch (err) {
      return next(asyncErrHandler(err))
  }
}