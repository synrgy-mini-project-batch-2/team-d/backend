const stripe = require('stripe')('sk_test_51ITONIKHg295YkwGhO4rFyOQzunF6wRz26nwX9k4F5BMFR8uUzhuMGV5Tix3ffBocEY7gefiHG46y17hQLeNJVEI000bSDqE0d');

const { customAlphabet } = require('nanoid');
const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 10);

const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { Courses } = require('../models');

exports.postPaymentIntent = async (req, res, next) => {
  const { id } = req.body;

  try {
    const fetchedCourse = await Courses.findOne({where: { id }, attributes: [ 'id', 'title', 'previewUrl', 'price' ]});


    if(!fetchedCourse){
      const error = createErrHandler(404, 'Course not found');
      return next(error)
    }
    

    const paymentIntent = await stripe.paymentIntents.create({
      amount: +fetchedCourse.price * 100,
      currency: 'IDR',
      payment_method_types: ['card']
    });

    return res.status(200).json({
      success: true,
      message: 'Payment intent created',
      clientSecret: paymentIntent.client_secret,
      course: fetchedCourse
    })

  } catch (err) {
    return next(asyncErrHandler(err))
  }
}

exports.postPaymentComplete = async (req, res, next) => {
  const { id } = req.body;

  const fetchedCourse = await Courses.findOne({where: { id }})
  const fetchedStudent = await req.user.getStudent();

    if(!fetchedStudent){
      const error = createErrHandler(404, 'Student not found');
      return next(error);
    }

    const purchaseId = nanoid();

    await fetchedStudent.addCourse(fetchedCourse, { through: { isOwned: true } });
    
    await fetchedStudent.createPurchaseHistory({
      courseId: fetchedCourse.id,
      purchaseId
    })

    return res.status(200).json({
      success: true,
      message: 'Payment successful'
    })
}