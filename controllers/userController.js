const { Users, Roles } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { hashPassword } = require('../helpers/bcryptHelper');
const { validationError } = require('../helpers/validationErrorHelper')

exports.getAllUsers = async (req, res, next) => {
  try {
    if(req.query.role){
      const role = req.query.role;

      const fetchedUsers = await Users.findAll({
        include: [{
          model: Roles,
          where: { rolename: role }
        }]
      });

      if(!fetchedUsers){
        return next(createErrHandler(404, 'No Users with such role are found'));
      }
      return res.status(200).json({
        success: true,
        users: fetchedUsers
      })
    }

    const fetchedUsers = await Users.findAll({
      include: [ Roles ]
    });
  
    return res.status(200).json({
      success: true,
      users: fetchedUsers
    })
  } catch (err) {
    return next(asyncErrHandler(err));
  }
  
}

exports.getUserById = async (req, res, next) =>{
  if(req){
    const id = req.params.id;

    try {
      const fetchedUser = await Users.findOne({ where: {id}, include: [ Roles ] });

      if(!fetchedUser){
        return next(createErrHandler(404, 'User not found'));
      }

      return res.status(200).json({
        success: true,
        user: fetchedUser
      })
    } catch (err) {
      return next(asyncErrHandler(err));
    }
  }
}

exports.postAdmin = async (req, res, next) => {
  if(validationError(req)){
    return next(validationError(req));
  }
  if(req){
    const { email, password } = req.body;

    const hashedPassword = hashPassword(password);

    try {
      const admin = await Roles.findOne({ where: { rolename: "admin" } });

      await admin.createUser({
        email,
        password: hashedPassword
      })

      return res.status(202).json({
        success: true,
        message: 'Admin created successfully'
      })
    } catch (err) {
      return next(asyncErrHandler(err));
    }
  }
}

exports.putUser = async (req, res, next) => {
  if(validationError(req)){
    return next(validationError(req))
  }
  
 
  const id = req.params.id;
  try {
    const fetchedUser = await Users.findOne({ where: { id }})

    if(!fetchedUser){
      return next(createErrHandler(404, 'User not found'))
    }

    const hashedPassword = hashPassword(password);

    await fetchedUser.update({
      email,
      password: hashedPassword
    })

    return res.status(200).json({
      success: true,
      message: `User with id ${id} successfully updated`
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}

exports.deleteUserById = async (req, res, next) => {
  const id = req.params.id;

  try {
    await Users.destroy({where: {id}});

    return res.status(200).json({
      success: true,
      message: `User with id ${id} successfully deleted`
    })
  } catch (err) {
    return next(asyncErrHandler(err))
  }
}