const { Recipes, Users } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { validationError } = require('../helpers/validationErrorHelper');
const bucket = require('../config/firebase')

exports.getAllrecipes = async(req, res, next) => {
    try {
        const fetchedRecipes =
            await Recipes.findAll({ attributes: [ 'id', 'title', 'imageUrl', 'createdAt' ] });

        if (!fetchedRecipes) {
            return next(createErrHandler(404, 'No recipes found'));
        }
        return res.status(200).json({
            success: true,
            data: fetchedRecipes
        })
    } catch (error) {
        return next(asyncErrHandler(error));
    }

}

exports.getRecipeById = async(req, res, next) => {
    if (req) {
        const id = req.params.id;
        try {
            const fetchedRecipes = await Recipes.findOne({
                where: { id },
                include: Users
            });

            if (!fetchedRecipes) {
                return next(createErrHandler(404, 'Recipe not found'));
            }

            return res.status(200).json({
                success: true,
                data: fetchedRecipes
            })
        } catch (err) {
            return next(asyncErrHandler(err));
        }
    }
}

exports.postRecipe = async(req, res, next) => {
    if (validationError(req)) {
        return next(validationError(req))
    }

    if(!req.file){
        const error = createErrHandler(422, 'No file picked');
        return next(error);
    }

    const { title, ingredients, directions, portions, duration } = req.body

    const image = req.file.originalname;
    let publicUrl;
    const blob = bucket.file(image);

    const blobStream = blob.createWriteStream({
        metadata: {
            contentType: req.file.mimetype
        }
    });

    blobStream.on('error', err => {
        return next(err);
    });

    blobStream.on('finish', async () => {
        publicUrl = `https://firebasestorage.googleapis.com/v0/b/${
                bucket.name
            }/o/${encodeURI(blob.name)}?alt=media`;
    })
    try {
        await req.user.createRecipe({
            title,
            ingredients,
            directions,
            portions,
            duration,
            imageUrl: publicUrl
        })

        return res.status(202).json({
            success: true,
            message: `Recipe created successfully`
        })
    } catch (err) {
        return next(asyncErrHandler(err));
    }
}

exports.putRecipeById = async(req, res, next) => {
    if (validationError(req)) {
        return next(validationError(req))
    }
    const id = req.params.id;
    const { title, ingredients, directions, portions, duration, imageUrl } = req.body
    try {
        const fetchedRecipes = await Recipes.findOne({ where: { id } })

        if (!fetchedRecipes) {
            return next(createErrHandler(404, 'Recipe not found'))
        }
        await fetchedRecipes.update({
            title,
            ingredients,
            directions,
            portions,
            duration,
            imageUrl
        })

        return res.status(200).json({
            success: true,
            message: `Recipe with id ${id} successfully updated`
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}

exports.deleteRecipeById = async(req, res, next) => {
    const id = req.params.id;

    try {
        await Recipes.destroy({ where: { id } });

        return res.status(200).json({
            success: true,
            message: `Recipe with id ${id} successfully deleted`
        })
    } catch (err) {
        return next(asyncErrHandler(err))
    }
}