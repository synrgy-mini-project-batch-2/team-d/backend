const router = require('express').Router();

const { articlesPostValidator, articlesPutValidator } = require('../../../middlewares/validations/dataValidator');
const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin')
const upload = require('../../../middlewares/upload');

const {getDates, getArticles, getArticleById, postArticle, putArticle, deleteArticleById  } = require('../../../controllers/articleController');



router.get('/', getArticles);
// router.get('/date', getDates);
router.get('/:id', getArticleById);
router.post('/',  isAuth, isAdmin, upload.single('image'), articlesPostValidator, postArticle)
router.put('/:id', isAuth, isAdmin, upload.single('image'), articlesPutValidator, putArticle)
router.delete('/:id', isAuth, isAdmin, deleteArticleById)

module.exports = router;