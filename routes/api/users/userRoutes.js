const router = require('express').Router();

const { getAllUsers, getUserById, postAdmin, putUser, deleteUserById } = require('../../../controllers/userController');

const { adminPostValidator, adminPutValidator } = require('../../../middlewares/validations/dataValidator');

const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin')


router.get('/', getAllUsers);
router.get('/:id', getUserById);
router.post('/', isAuth, isAdmin, adminPostValidator, postAdmin)
router.put('/:id', isAuth, isAdmin, adminPutValidator, putUser)
router.delete('/:id', isAuth, isAdmin, deleteUserById)

module.exports = router;