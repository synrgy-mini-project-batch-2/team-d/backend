const router = require('express').Router();

const { getAllStudents, getStudentById, getStudentInfo, putStudentById, postStudentPassword, putStudentPassword, deleteStudentById } = require('../../../controllers/studentController')
const { studentPutValidator } = require('../../../middlewares/validations/dataValidator');
const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin');
const upload = require('../../../middlewares/upload');


router.get('/', isAuth, isAdmin, getAllStudents);
router.get('/get-student-by-id/:id', isAuth, getStudentById);
router.get('/get-student-info', isAuth, getStudentInfo)
router.put('/:id', isAuth, upload.single('image'), studentPutValidator, putStudentById);
router.post('/verify-password', isAuth, postStudentPassword);
router.put('/change-password/', isAuth, putStudentPassword)
router.delete('/:id', isAuth, deleteStudentById);

module.exports = router;