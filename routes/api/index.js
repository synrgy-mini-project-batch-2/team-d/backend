const router = require('express').Router();

const { errorHandler } = require('../../middlewares/errorHandler');

const recipeRoutes = require('./recipes/recipeRoutes')
const authRoutes = require('./auth/authRoutes');
const userRoutes = require('./users/userRoutes');
const articleRoutes = require('./articles/articlesRoutes')
const courseRoutes = require('./courses/courseRoutes')
const purchaseHistorysRoutes = require('./purchaseHistorys/purchaseHistorysRoutes')
const reviewRoutes = require('./reviews/reviewRoutes')
const studentRoutes = require('./students/studentRoutes');
const teacherRoutes = require('./teachers/teacherRoutes');
const checkoutRoutes = require('./checkout/checkoutRoutes')

router.use('/auth', authRoutes);
router.use('/users', userRoutes);
router.use('/recipes', recipeRoutes)
router.use('/articles', articleRoutes);
router.use('/courses', courseRoutes);
router.use('/purchase-histories', purchaseHistorysRoutes);
router.use('/reviews', reviewRoutes);
router.use('/students', studentRoutes);
router.use('/teachers', teacherRoutes);
router.use('/checkout', checkoutRoutes);
router.use(errorHandler);

module.exports = router;