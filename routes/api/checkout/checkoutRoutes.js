const router = require('express').Router();

const { postPaymentIntent, postPaymentComplete } = require('../../../controllers/checkoutController');
const isAuth = require('../../../middlewares/isAuth');

router.post('/create-payment-intent', isAuth, postPaymentIntent);
router.post('/complete-payment', isAuth, postPaymentComplete);

module.exports = router;