const router = require('express').Router();

const { loginHandler, registerHandler, verifyEmail, forgotPassword, putPassword } = require('../../../controllers/authController')
const { loginValidator, registerValidator } = require('../../../middlewares/validations/dataValidator');
const isAuth = require('../../../middlewares/isAuth');

router.post('/login', loginValidator, loginHandler);
router.post('/register', registerValidator, registerHandler);

router.put('/verify-email/:token', isAuth, verifyEmail);
router.put('/forgot-password', forgotPassword);
router.put('/reset-password/:token', putPassword)
module.exports = router;