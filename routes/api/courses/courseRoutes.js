const router = require('express').Router();

const { coursesPostValidator, coursesPutValidator } = require('../../../middlewares/validations/dataValidator')

const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin');
const getUser = require('../../../middlewares/getUser');
const upload = require('../../../middlewares/upload');

//load controller
const { getCourses, getCoursesById, getCoursesByRegion, getCoursesBySearch, createCourses, putCourses, deleteCoursesById, getOwnedCourses, getOwnedCourseById } = require("../../../controllers/courseController");

//set endpoint url class
router.get("/", getCourses);
router.get("/:id", getUser, getCoursesById);
router.get("/get-by-region", getCoursesByRegion);
router.get("/get-by-search", getCoursesBySearch);
router.post("/", isAuth, isAdmin, upload.array('medias', 2), coursesPostValidator, createCourses);
router.put("/:id", isAuth, isAdmin, upload.array('medias', 2), coursesPutValidator,  putCourses);
router.delete("/:id", isAuth, isAdmin, deleteCoursesById);
router.get("/get-owned-course/get-all", isAuth, getOwnedCourses);
router.get('/get-owned-course-by-id/:id', isAuth, getOwnedCourseById);

module.exports = router;