const router = require('express').Router();
const { getReviews, getReviewById, postReview, putReviewById, deleteReviewById } = require('../../../controllers/reviewController');
const { reviewsPostValidator, reviewsPutValidator } = require('../../../middlewares/validations/dataValidator')
const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin');

router.get('/', getReviews);
router.get('/:id', getReviewById);
router.post('/', isAuth, reviewsPostValidator, postReview);
router.put('/:id', isAuth, putReviewById);
router.delete('/:id', isAuth, deleteReviewById);

module.exports = router;