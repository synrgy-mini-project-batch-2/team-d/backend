const router = require('express').Router();
const { getAllrecipes, getRecipeById, postRecipe, putRecipeById, deleteRecipeById } = require('../../../controllers/recipeController');
const { recipesPostValidator, recipesPutValidator } = require('../../../middlewares/validations/dataValidator')
const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin');
const upload = require('../../../middlewares/upload');

router.get('/', getAllrecipes);
router.get('/:id', getRecipeById);
router.post('/', isAuth, isAdmin, upload.single('image'), recipesPostValidator, postRecipe);
router.put('/:id', isAuth, isAdmin, recipesPutValidator, putRecipeById);
router.delete('/:id', isAuth, isAdmin, deleteRecipeById);

module.exports = router;