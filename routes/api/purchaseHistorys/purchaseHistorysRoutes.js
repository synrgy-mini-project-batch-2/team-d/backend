const router = require('express').Router();
const { getAllPurchaseHistorys, getPurchaseHistoryById, postPurchaseHistorys, putPurchaseHistoryById, deletePurchaseHistoryById } = require('../../../controllers/purchaseHistorysController');
// const { purchaseHistorysPostValidator, purchaseHistorysPutValidator } = require('../../../middlewares/validations/dataValidator')
const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin');

router.get('/', isAuth, getAllPurchaseHistorys);
router.get('/:id', isAuth, getPurchaseHistoryById);
router.post('/', isAuth, isAdmin, postPurchaseHistorys);
router.put('/:id', isAuth, isAdmin, putPurchaseHistoryById);
router.delete('/:id', isAuth, isAdmin, deletePurchaseHistoryById);

module.exports = router;