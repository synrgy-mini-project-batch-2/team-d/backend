const router = require('express').Router();

const { getAllTeachers, getTeacherById, postTeacher, putTeacherById, deleteTeacherById } = require('../../../controllers/teacherController');
const { teacherPostValidator, teacherPutValidator } = require('../../../middlewares/validations/dataValidator');

const isAuth = require('../../../middlewares/isAuth');
const isAdmin = require('../../../middlewares/isAdmin');

router.get('/', isAuth, isAdmin, getAllTeachers);
router.get('/:id', isAuth, isAdmin, getTeacherById);
router.post('/', isAuth, isAdmin, teacherPostValidator, postTeacher);
router.put('/:id', isAuth, isAdmin, teacherPutValidator, putTeacherById);
router.delete('/:id', isAuth, isAdmin, deleteTeacherById);

module.exports = router;