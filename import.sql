--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Articles; Type: TABLE; Schema: public; Owner: testuser
--

CREATE TABLE public."Articles" (
    id integer NOT NULL,
    title character varying(255),
    content text,
    "imageUrl" character varying(255),
    "userId" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Articles" OWNER TO anandailham;

--
-- Name: Articles_id_seq; Type: SEQUENCE; Schema: public; Owner: anandailham
--

CREATE SEQUENCE public."Articles_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Articles_id_seq" OWNER TO anandailham;

--
-- Name: Articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: anandailham
--

ALTER SEQUENCE public."Articles_id_seq" OWNED BY public."Articles".id;


--
-- Name: Courses; Type: TABLE; Schema: public; Owner: anandailham
--

CREATE TABLE public."Courses" (
    id integer NOT NULL,
    title character varying(255),
    content text,
    "videoUrl" character varying(255),
    "userId" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Courses" OWNER TO anandailham;

--
-- Name: Courses_id_seq; Type: SEQUENCE; Schema: public; Owner: testuser
--

CREATE SEQUENCE public."Courses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Courses_id_seq" OWNER TO anandailham;

--
-- Name: Courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: anandailham
--

ALTER SEQUENCE public."Courses_id_seq" OWNED BY public."Courses".id;


--
-- Name: Recipes; Type: TABLE; Schema: public; Owner: anandailham
--

CREATE TABLE public."Recipes" (
    id integer NOT NULL,
    title character varying(255),
    content text,
    "userId" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Recipes" OWNER TO anandailham;

--
-- Name: Recipes_id_seq; Type: SEQUENCE; Schema: public; Owner: anandailham
--

CREATE SEQUENCE public."Recipes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Recipes_id_seq" OWNER TO anandailham;

--
-- Name: Recipes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: anandailham
--

ALTER SEQUENCE public."Recipes_id_seq" OWNED BY public."Recipes".id;


--
-- Name: Roles; Type: TABLE; Schema: public; Owner: anandailham
--

CREATE TABLE public."Roles" (
    id integer NOT NULL,
    rolename character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Roles" OWNER TO anandailham;

--
-- Name: Roles_id_seq; Type: SEQUENCE; Schema: public; Owner: anandailham
--

CREATE SEQUENCE public."Roles_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Roles_id_seq" OWNER TO anandailham;

--
-- Name: Roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: anandailham
--

ALTER SEQUENCE public."Roles_id_seq" OWNED BY public."Roles".id;


--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: anandailham
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO anandailham;

--
-- Name: StudentCourses; Type: TABLE; Schema: public; Owner: testuser
--

CREATE TABLE public."StudentCourses" (
    id integer NOT NULL,
    "studentId" integer,
    "courseId" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."StudentCourses" OWNER TO anandailham;

--
-- Name: StudentCourses_id_seq; Type: SEQUENCE; Schema: public; Owner: anandailham
--

CREATE SEQUENCE public."StudentCourses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."StudentCourses_id_seq" OWNER TO anandailham;

--
-- Name: StudentCourses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owneranandailhamer
--

ALTER SEQUENCE public."StudentCourses_id_seq" OWNED BY public."StudentCourses".id;


--
-- Name: Students; Type: TABLE; Schema: public; Owner: testuser
--

CREATE TABLE public."Students" (
    id integer NOT NULL,
    name character varying(255),
    "userId" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Students" OWNER TO anandailham;

--
-- Name: Students_id_seq; Type: SEQUENCE; Schema: public; Owner: testuser
--

CREATE SEQUENCE public."Students_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Students_id_seq" OWNER TO anandailham;

--
-- Name: Students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: testuser
--

ALTER SEQUENCE public."Students_id_seq" OWNED BY public."Students".id;


--
-- Name: Users; Type: TABLE; Schema: public; Owner: testuser
--

CREATE TABLE public."Users" (
    id integer NOT NULL,
    email character varying(255),
    password character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "roleId" integer
);


ALTER TABLE public."Users" OWNER TO anandailham;

--
-- Name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: testuser
--

CREATE SEQUENCE public."Users_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Users_id_seq" OWNER TO anandailham;

--
-- Name: Users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: testuser
--

ALTER SEQUENCE public."Users_id_seq" OWNED BY public."Users".id;


--
-- Name: Articles id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Articles" ALTER COLUMN id SET DEFAULT nextval('public."Articles_id_seq"'::regclass);


--
-- Name: Courses id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Courses" ALTER COLUMN id SET DEFAULT nextval('public."Courses_id_seq"'::regclass);


--
-- Name: Recipes id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Recipes" ALTER COLUMN id SET DEFAULT nextval('public."Recipes_id_seq"'::regclass);


--
-- Name: Roles id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Roles" ALTER COLUMN id SET DEFAULT nextval('public."Roles_id_seq"'::regclass);


--
-- Name: StudentCourses id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."StudentCourses" ALTER COLUMN id SET DEFAULT nextval('public."StudentCourses_id_seq"'::regclass);


--
-- Name: Students id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Students" ALTER COLUMN id SET DEFAULT nextval('public."Students_id_seq"'::regclass);


--
-- Name: Users id; Type: DEFAULT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Users" ALTER COLUMN id SET DEFAULT nextval('public."Users_id_seq"'::regclass);


--
-- Data for Name: Articles; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."Articles" (id, title, content, "imageUrl", "userId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Courses; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."Courses" (id, title, content, "videoUrl", "userId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Recipes; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."Recipes" (id, title, content, "userId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Roles; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."Roles" (id, rolename, "createdAt", "updatedAt") FROM stdin;
1	admin	2021-02-25 13:35:56.155+07	2021-02-25 13:35:56.155+07
\.


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."SequelizeMeta" (name) FROM stdin;
20210225061854-create-role.js
20210225061712-create-user.js
20210225063648-create-recipe.js
20210225063920-create-articles.js
20210225064212-create-students.js
20210225064415-create-courses.js
20210225064451-create-student-course.js
\.


--
-- Data for Name: StudentCourses; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."StudentCourses" (id, "studentId", "courseId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Students; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."Students" (id, name, "userId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: testuser
--

COPY public."Users" (id, email, password, "createdAt", "updatedAt", "roleId") FROM stdin;
2	asd@gmail.com	goblok	2021-02-25 13:35:56.146+07	2021-02-25 13:35:56.146+07	\N
\.


--
-- Name: Articles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."Articles_id_seq"', 1, false);


--
-- Name: Courses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."Courses_id_seq"', 1, false);


--
-- Name: Recipes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."Recipes_id_seq"', 1, false);


--
-- Name: Roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."Roles_id_seq"', 1, true);


--
-- Name: StudentCourses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."StudentCourses_id_seq"', 1, false);


--
-- Name: Students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."Students_id_seq"', 1, false);


--
-- Name: Users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: testuser
--

SELECT pg_catalog.setval('public."Users_id_seq"', 2, true);


--
-- Name: Articles Articles_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Articles"
    ADD CONSTRAINT "Articles_pkey" PRIMARY KEY (id);


--
-- Name: Courses Courses_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Courses"
    ADD CONSTRAINT "Courses_pkey" PRIMARY KEY (id);


--
-- Name: Recipes Recipes_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Recipes"
    ADD CONSTRAINT "Recipes_pkey" PRIMARY KEY (id);


--
-- Name: Roles Roles_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY (id);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: StudentCourses StudentCourses_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."StudentCourses"
    ADD CONSTRAINT "StudentCourses_pkey" PRIMARY KEY (id);


--
-- Name: Students Students_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Students"
    ADD CONSTRAINT "Students_pkey" PRIMARY KEY (id);


--
-- Name: Users Users_email_key; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_email_key" UNIQUE (email);


--
-- Name: Users Users_pkey; Type: CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- Name: Articles Articles_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Articles"
    ADD CONSTRAINT "Articles_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."Users"(id);


--
-- Name: Recipes Recipes_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Recipes"
    ADD CONSTRAINT "Recipes_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."Users"(id);


--
-- Name: StudentCourses StudentCourses_courseId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."StudentCourses"
    ADD CONSTRAINT "StudentCourses_courseId_fkey" FOREIGN KEY ("courseId") REFERENCES public."Courses"(id);


--
-- Name: StudentCourses StudentCourses_studentId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."StudentCourses"
    ADD CONSTRAINT "StudentCourses_studentId_fkey" FOREIGN KEY ("studentId") REFERENCES public."Students"(id);


--
-- Name: Students Students_userId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Students"
    ADD CONSTRAINT "Students_userId_fkey" FOREIGN KEY ("userId") REFERENCES public."Users"(id);


--
-- Name: Users Users_roleId_fkey; Type: FK CONSTRAINT; Schema: public; Owner: testuser
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_roleId_fkey" FOREIGN KEY ("roleId") REFERENCES public."Roles"(id);


--
-- PostgreSQL database dump complete
--

