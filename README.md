# IMPORTANT NOTES
## Branching
Ingat untuk melakukan development fitur tersendiri dengan branch yang berbeda dari **master**, **develop**, dan **production**. Pergunakan format berikut: **feature/nama-fitur**.

## List terminal commands penting
```bash
npm i #Install package dependencies project

sequelize db:migrate #Migrate pending migrations ke database

sequelize db:migrate:undo:all #Undo semua migrations (DROP TABLE)

sequelize db:seed --seed 20210227075910-table.js #Seeding specific seeder

sequelize db:seed:all #Jalankan semua seeders

sequelize db:seed:undo:all #Undo semua seeding (DELETE *)

sequelize seed:generate --name seeder-file #Generate seeder baru

sequelize model:generate --name ModelName --attributes name:string #Generate model baru beserta migrationnya

sequelize migration:generate --name migration-file #Generate file migration baru

npm start #Spin up server dengan nodemon (port default adalah 5000)
```

## Note untuk migrations with associations
Berikut merupakan step-by-step untuk melakukan migration beserta assocation pada tables:
1. Buat terlebih dahulu table yang akan menjadi foreign_key pada table lain dengan command 
```bash 
sequelize model:generate --name ModelName --attributes modelAttributes 
```
2. Jalankan 
```bash 
sequelize db:migrate 
``` 
untuk membuat table tersebut di database
3. Buatlah table-table yang akan menggunakan foreign_key tersebut dengan command yang sama dengan #1, yaitu 
```bash 
sequelize model:generate --name ModelName --attributes modelAttributes 
```
bedanya kali ini pada `attributes`, sertakan foreign_key dari parent, contohnya `userId:integer`

4. Definisikan juga relasi antara parent dan children tersebut pada file `model` masing-masing.
	contohnya: 
	```js
		static associate(models) {
      		// define association here
      		this.belongsTo(models.Users, {
        		foreignKey:'userId',
        		foreignKeyConstraint: true,
        		onDelete: 'SET NULL'
      })
    }
	```
5. Edit file migration untuk table baru tersebut dengan menuliskan referensi model parent yang menjadi foreign_key nya
	contohnya:
	```js
		userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
          as: 'userId'
        },
        onDelete: 'SET NULL'
      },
	```
6. Jika sudah yakin, maka jalankan 
```bash 
sequelize db:migrate 
```
7. Selesai

**Note: Apabila lupa atau ada attribute yang ingin ditambah, sebaiknya jangan** 
```bash 
sequelize db:migrate:undo:all 
```
**karena akan bermasalah nantinya ketika melakukan migrate ulang lagi.**

## Cloud database
Untuk database, saya sudah menyiapkan cloud database yang di hosting dengan **Heroku Postgres**. Penggunaannya cukup dengan pull dari branch **develop**, config untuk masuk ke database tersebut sudah saya sediakan.


## Pushing & Merge ke repo
Gunakan perintah:
```bash
git push -u origin feature/nama-fitur #Untuk push perdana
# atau
git push origin feature/nama-fitur #Untuk push kedua dan seterusnya
```
Ketika ingin melakukan push ke repo. Lakukan hal berikut ketika ingin melakukan merge request:

1. Pastikan **Title** memuat informasi yang deskriptif singkat terkait fitur yang sudah dibuat
2.  Pada **Description**, jelaskan dengan ringkas apa saja yang dibuat, beserta masalah yang ada (kalau ada).
3. Set **Assignees** ke **ErwinHaa**
4. Set **Reviewer** ke **ErwinHaa**
5. Skip **Milestone & Label**
6. **Merge Options** bisa dibiarkan ke setting default
	
Kabarkan ke saya apabila sudah melakukan merge request. Jika sudah oke, akan langsung di merge ke branch **develop**.

## Pulling dari repo
Jangan lupa untuk selalu melakukan 
```bash
git pull origin develop
```
Demi mencegah commit behind ketika ada merging ke branch **develop**. Setelah itu jika ingin melanjutkan development fitur baru, lakukan branching dari branch **develop** dengan perintah:
```bash
git checkout -b feature/nama-fitur
```

**Kalau ingin melakukan merge di local branch, perhatikan baik-baik merge conflictnya. Awas code hilang dicuri mahluk halus.**

## Migration untuk perubahan attributes pada table
Untuk melakukan migration, berikut step-step yang harus diberlakukan:
1. Buatlah sebuah file migration baru dengan cara:
```bash
sequelize migration:generate --name update-nama_table
```
2. Buka file migration baru tersebut, dan tambahkan code sesuai keinginan
```js

// Ini contoh code untuk menambah column baru
await queryInterface.addColumn('nama_table', 'nama_column', {
	type: DataTypes.APADATATYPENYA,
})

// Ini contoh code untuk mengubah column
await queryInterface.changeColumn('nama_table', 'nama_column', {
	type: DataTypes.APADATATYPEBARUNYA,
	allowNull: false,
})

// Ini contoh code untuk menghapus column
await queryInterface.removeColumn('nama_table', 'nama_column');
```
3. Jangan lupa untuk menambah/mengubah/menghapus datatypes yang berada di method `.init()` di model masing-masing yang mengalami perubahan, contohnya:
```js
NamaTable.init({
	columnLama: DataTypes.INIDATATYPE,
	columnLama: DataTypes.INIDATATYPE,
	columnBaru: DataTypes.INIDATATYPEYANGBARU
})
```
Kalau ini ga di berlakukan, maka ketika melakukan query `GET`, attribute tidak akan diikutsertakan, namun di table database tetap ada data tersebut.

4. Jika sudah pasti semuanya aman, jalankan
```bash
sequelize db:migrate
```
5. Jangan lupa kabari apabila ada membuat perubahan pada attributes supaya engineer BE tahu apa yang terjadi.

**Documentation: https://sequelize.org/master/manual/query-interface.html**

## Error Handling
Di folder `helpers` sudah ada file untuk error handling, yaitu `errorHelpers.js`. Penggunaannya:

Apabila tidak ada function yang menyimpan error (seperti `catch(err)`), pergunakan `createErrHandler` untuk membuat error baru.
```js
	const { createErrHandler } = require ('../helpers/errorHelpers')
	const error = createErrHandler(statusCode, message) // Contoh statusCode = 404, 422, 500, dll, Contoh message: 'User not found'
```

Sebaliknya apabila ada function yang menyimpan error (seperti `catch(err)`), pergunakan `asyncErrHandler` untuk forward error tersebut ke middleware error handling
```js
	.catch(err){
		return next(asyncErrHandler(err))
	}
```

Semua error akan di forward ke middleware `errorHandler.js`.

## Data Validation
Di folder `middlewares/validations`, sudah ada file `dataValidator.js` yang menyimpan array untuk berbagai macam data validations. Penggunaannya:
```js
const { body } = require('express-validator');

exports.iniAdalahValidator = [
	(body('field1'))  					// Field merupakan request body yang dikirim ke server dari client seperti email, password, dll.
	.notEmpty() 						// Cek apakah field tersebut ada atau nga
	.withMessage('Ini kok kosong') 		// Kasih message error yang nantinya bakal di forward ke error handler
	.trim()								// Trim whitespaces dari request body
	.escape(),							// Ignore semua tag HTML yang mungkin saja dapat disisip ke request body untuk mencegah XSS attack.
	(body('field2'))
	.notEmpty()
	.withMessage('Ini jangan kosong')
	.trim()
	.escape()
]
```
Gunakan validator tersebut layaknya sebuah middleware pada routes
```js
const router = require('express').Router();
const { iniAdalahValidator } = require('../middlewares/validations/dataValidator');
const { postHandler } = require('../controllers/iniController')

router.post('/', iniAdalahValidator, postHandler)

module.exports = router
```
**Note: Data Validation biasanya hanya digunakan pada method POST dan PUT, dimana ada request body yang dikirimkan ke server untuk di validasi dan di sanitasi.**

Jangan lupa pada controller terkait untuk menyertakan `validationError` yang diambil dari `helpers/validationErrorHelper.js`
`iniController.js`
```js
const { validationError } = require('../helpers/validationErrorHelper')

exports.postHandler = (req, res, next) => {
	if(validationError(req)){  // Jika ada error yang di forward dari middleware validator
		return next(validationError(req))  // Forward error tersebut ke error handler middleware
	}
}
```

**Documentation: https://www.npmjs.com/package/validator** (express-validator di bangun dari validator.js ini)


### Side note
Jangan lupa untuk mengupdate **trello** ketika ada fitur yang selesai atau apabila ada kendala guna menjaga koordinasi antar tim yang teratur ^^.
