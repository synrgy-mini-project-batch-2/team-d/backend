'use strict';

const bcrypt = require('bcryptjs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   const hashPassword = (password) => {
     return bcrypt.hashSync(password, 12)
   }
   await queryInterface.bulkInsert('Roles', [{
    rolename: 'admin',
    createdAt: new Date(),
    updatedAt: new Date()
  }, {
    rolename: 'default',
    createdAt: new Date(),
    updatedAt: new Date()
  }])

      const roles = await queryInterface.sequelize.query(`SELECT id from "Roles";`);

      const rolesRows = roles[0];

      await queryInterface.bulkInsert('Users', [{
        email: 'admin@gmail.com',
        password: hashPassword('iniadmincuy'),
        createdAt: new Date(),
        updatedAt: new Date(),
        roleId: rolesRows[0].id
      }, {
        email: 'jaston@gmail.com',
        password: hashPassword('inistudentcuy'),
        createdAt: new Date(),
        updatedAt: new Date(),
        roleId: rolesRows[1].id
      },
      {
        email: 'saria@gmail.com',
        password: hashPassword('inistudentcuy'),
        createdAt: new Date(),
        updatedAt: new Date(),
        roleId: rolesRows[1].id
      }])

      const users = await queryInterface.sequelize.query(`SELECT id from "Users";`);

      const usersRows = users[0];

      await queryInterface.bulkInsert('Students', [{
        firstName: 'Jaston',
        lastName: 'Williams',
        imageUrl: 'https://images-na.ssl-images-amazon.com/images/I/41tKs-+D6aL._SX331_BO1,204,203,200_.jpg',
        userId: usersRows[1].id,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        firstName: 'Saria',
        imageUrl: 'https://static.wikia.nocookie.net/meme/images/d/d5/Swag_Cat.jpg/revision/latest?cb=20200611194419',
        userId: usersRows[2].id,
        createdAt: new Date(),
        updatedAt: new Date()
      }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
    await queryInterface.bulkDelete('Roles', null, {});
    await queryInterface.bulkDelete('Students', null, {});
  }
};
