'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const users = await queryInterface.sequelize.query('SELECT "Users"."id", "Users"."roleId", "Role"."id" AS "Role.id" FROM "Users" AS "Users" INNER JOIN "Roles" AS "Role" ON "Users"."roleId" = "Role"."id" AND "Role"."rolename" = \'admin\' ;');

    const usersRows = users[0];

    await queryInterface.bulkInsert('Articles', [{
      title: 'Ini title 1',
      content: 'Ini content 1',
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/c/c7/Doritos_and_Guacamole_in_a_wooden_bowl.JPG',
      userId: usersRows[0].id,
      createdAt: new Date(),
      updatedAt: new Date()
    }, 
    {
      title: 'Ini title 2',
      content: 'Ini content 2',
      imageUrl: 'https://i.redd.it/vqco9k5ufp751.jpg',
      userId: usersRows[0].id,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      title: 'Ini title 3',
      content: 'Ini content 3',
      imageUrl: 'https://i.ytimg.com/vi/9BKcjBaKRkk/maxresdefault.jpg',
      userId: usersRows[1].id,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      title: 'Ini title 4',
      content: 'Ini content 4',
      imageUrl: 'https://hips.hearstapps.com/vidthumb/images/delish-u-rice-2-1529079587.jpg?crop=1.00xw:0.893xh;0,0.0580xh&resize=1200:*',
      userId: usersRows[1].id,
      createdAt: new Date(),
      updatedAt: new Date()
    }])

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
