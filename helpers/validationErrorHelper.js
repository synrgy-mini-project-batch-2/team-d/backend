const { validationResult } = require('express-validator');
const { createErrHandler } = require('../helpers/errorHelpers');

exports.validationError = req => {
  const errors = validationResult(req);
  if(!errors.isEmpty()){
    const data = errors.array();
    return createErrHandler(422, 'Validation Error', data);
  }
  return false;
}