const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

const sendEmail = async (email, subject, payload, template) => {
  try {
    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      secure: true,
      auth: {
        user: "cs.mamasakan@gmail.com",
        pass: "Iniadalahpassword"
      },
      tls: {
        rejectUnauthorized: false
      }
    })

    const source = fs.readFileSync(path.join(__dirname, 'email', template), "utf-8");
    const compiledTemplate = handlebars.compile(source);
    const options = () => {
      return {
        from: 'test@gmail.com',
        to: email,
        subject,
        html: compiledTemplate(payload)
      }
    }

    transporter.sendMail(options(), (err, info) => {
      if(err){
        throw err;
      }
      return res.status(200).json({
        success: true
      })
    })
  } catch (err) {
    throw err;
  }
}

module.exports = sendEmail;