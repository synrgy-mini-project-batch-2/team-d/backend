const createError = require('http-errors');

exports.asyncErrHandler = (err) => {
  if(!err.statusCode){
    err.statusCode = 500
  }
  return err;
}

exports.createErrHandler = (statusCode, message, data = []) => {
  const error = createError(statusCode, message);
  if(data.length){
    error.data = data;
  }
  return error;
}