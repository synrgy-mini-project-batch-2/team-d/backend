const jwt = require('jsonwebtoken');
const { createErrHandler } = require('./errorHelpers');
const { ACCESS_TOKEN_SECRET, REFRESH_TOKEN_SECRET, VERIFY_TOKEN_SECRET, RESET_TOKEN_SECRET } = process.env

const accessTokenOptions = {
  expiresIn: '1y'
}

const refreshTokenOptions = {
  expiresIn: '1y'
}

const verifyTokenOptions = {
  expiresIn: '1h'
}

const resetTokenOptions = {
  expiresIn: '15m'
}

exports.generateAccessToken = payload => jwt.sign(payload, ACCESS_TOKEN_SECRET, accessTokenOptions);

exports.generateRefreshToken = payload => jwt.sign(payload, REFRESH_TOKEN_SECRET, refreshTokenOptions);

exports.generateVerifyToken = payload => jwt.sign(payload, VERIFY_TOKEN_SECRET, verifyTokenOptions);

exports.generateResetToken = payload => jwt.sign(payload, RESET_TOKEN_SECRET, resetTokenOptions)

exports.verifyActivationToken = token => jwt.verify(token, VERIFY_TOKEN_SECRET, (err, payload) => {
  if(err){
    const error = createErrHandler(401, 'Incorrect token or expired');
    throw error;
  }
}) 

exports.verifyResetToken = token => jwt.verify(token, RESET_TOKEN_SECRET, (err, payload) => {
  if(err){
    const error = createErrHandler(401, 'Incorrectg token or expired');
    throw error;
  }
})