const bcrypt = require('bcryptjs');

exports.hashPassword = password => bcrypt.hashSync(password, 12);

exports.comparePass = (password, hashedPassword) => bcrypt.compareSync(password, hashedPassword);