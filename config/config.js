require('dotenv').config();
const { DATABASE_URL } = process.env

module.exports = {
    "development": {
        // "username": DB_USER,
        // "password": DB_PASS,
        // "database": DB_NAME,
        // "host": DB_HOST,
        "use_env_variable": "DATABASE_URL",
        "dialect": "postgres",
        "dialectOptions": {
            "ssl": {
                "require": true,
                "rejectUnauthorized": false
            }
        }
    },
    "test": {
        "username": "root",
        "password": null,
        "database": "database_test",
        "host": "127.0.0.1",
        "dialect": "mysql"
    },
    "production": {
        "use_env_variable": "DATABASE_URL",
        "dialect": "postgres",
        "dialectOptions": {
            "ssl": true,
            "rejectUnauthorized": false
        }
    }
}