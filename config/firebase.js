const { Storage } = require('@google-cloud/storage');
require('dotenv').config()


const {
  GCLOUD_PROJECT_ID,
  GCLOUD_KEY,
  GCLOUD_STORAGE_BUCKET_URL,
} = process.env;

const storage = new Storage({
  projectId: GCLOUD_PROJECT_ID,
  keyFilename: 'services/privateKey.json',
});

const bucket = storage.bucket(GCLOUD_STORAGE_BUCKET_URL);

module.exports = bucket;
