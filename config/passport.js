const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt');
const { Users, Roles, Students } = require('../models');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');
const { ACCESS_TOKEN_SECRET } = process.env

require('dotenv').config();
module.exports = (passport) => {
  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: ACCESS_TOKEN_SECRET
  }

  passport.use(new JwtStrategy(options, async (payload, next) => {
    const id = payload.userId;
    try {
      const fetchedUser = await Users.findOne({ where: { id }, include: {
        'model': Roles,
        attributes: 
          ['rolename']
      } });

      if(!fetchedUser){
        return next(createErrHandler(404, 'User not found'), false)
      }

      // const role = await fetchedUser.getRole();
      return next(null, fetchedUser);

    } catch (err) {
      return next(asyncErrHandler(err), false);
    }
  }))
}