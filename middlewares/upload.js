const multer = require('multer');

const storage = multer.memoryStorage();

const fileFilter = (req, file, next) => {
  if (
    file.mimetype === 'video/mp4' ||
    file.mimetype === 'video/avi' ||
    file.mimetype === 'video/mov' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg' ||
    file.mimetype === 'image/png'
  ) {
    next(null, true);
  } else {
    next(null, false);
  }
};

const upload = multer({ storage, fileFilter });

module.exports = upload;
