const { createErrHandler } = require('../helpers/errorHelpers');

module.exports = (req, res, next) => {
  const { user } = req;

  const role = user.Role.rolename;

  if(role !== 'admin'){
    const error = createErrHandler(401, 'Unauthorized')
    throw error;
  }
  next();
}