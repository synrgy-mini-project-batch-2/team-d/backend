const passport = require('passport');

module.exports = (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if(!user){
      req.user = null;
      return next();
    }
    req.user = user;
    return next()
  })(req, res, next)
}