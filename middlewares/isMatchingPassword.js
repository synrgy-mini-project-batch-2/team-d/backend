const { comparePass } = require('../helpers/bcryptHelper');
const { createErrHandler } = require('../helpers/errorHelpers');

const checkPassword = async (req, res, next) => {
  const { password } = req.body;

  const compare = comparePass(password, req.user.password);
  if(!compare){
    const error = createErrHandler(400, 'Password doesn\'t match');
    throw error;
  }
  next();
}

module.exports = checkPassword