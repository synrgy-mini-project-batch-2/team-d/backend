const { body } = require('express-validator');
const { Users, Teachers } = require('../../models');
const { comparePass } = require('../../helpers/bcryptHelper');
const { createErrHandler } = require('../../helpers/errorHelpers');

exports.adminPostValidator = [
    (body('email'))
    .notEmpty()
    .withMessage('Email cannot be empty')
    .isEmail()
    .withMessage('Invalid Email format')
    .custom(async value => {
        const fetchedUser = await Users.findOne({ where: { email: value } })
        if (fetchedUser) {
            throw createErrHandler(400, 'Email already exists!')
        }
    })
    .trim()
    .escape()
    .normalizeEmail(),
    (body('password'))
    .notEmpty()
    .withMessage('Password cannot be empty!')
    .isStrongPassword({
        minLowerCase: 1,
        minUpperCase: 0,
        minSymbols: 0,
        minLength: 6,
    })
    .withMessage('Password\'s strength is too weak.')
    .trim()
    .escape()
]

exports.adminPutValidator = [
    (body('email'))
    .notEmpty()
    .withMessage('Email cannot be empty')
    .normalizeEmail()
    .isEmail()
    .withMessage('Invalid Email format')
    .trim()
    .escape(),
    (body('password'))
    .notEmpty()
    .withMessage('Password cannot be empty')
    .isStrongPassword({
        minLowerCase: 1,
        minUpperCase: 0,
        minSymbols: 0,
        minLength: 6,
    })
    .withMessage('Password\'s strength is too weak')
    .trim()
    .escape()
]

exports.recipesPostValidator = [
    (body('title'))
    .notEmpty()
    .withMessage('Title cannot be empty')
    .trim()
    .escape(),
    (body('ingredients'))
    .notEmpty()
    .withMessage('Ingredients cannot be empty')
    .trim()
    .escape(),
    (body('directions'))
    .notEmpty()
    .withMessage('Directions cannot be empty')
    .trim()
    .escape(),
    (body('portions'))
    .notEmpty()
    .withMessage('Portions cannot be empty')
    .trim()
    .escape(),
    (body('duration'))
    .notEmpty()
    .withMessage('Duration cannot be empty')
    .trim()
    .escape()
]

exports.recipesPutValidator = [
    (body('title'))
    .notEmpty()
    .withMessage('Title cannot be empty')
    .trim()
    .escape(),
    (body('ingredients'))
    .notEmpty()
    .withMessage('Ingredients cannot be empty')
    .trim()
    .escape(),
    (body('directions'))
    .notEmpty()
    .withMessage('Directions cannot be empty')
    .trim()
    .escape(),
    (body('portions'))
    .notEmpty()
    .withMessage('Portions cannot be empty')
    .trim()
    .escape(),
    (body('duration'))
    .notEmpty()
    .withMessage('Duration cannot be empty')
    .trim()
    .escape()
]

exports.loginValidator = [
    (body('email'))
    .notEmpty()
    .withMessage('Email cannot be empty')
    .normalizeEmail()
    .isEmail()
    .withMessage('Invalid email format')
    .custom(async value => {
        const fetchedUser = await Users.findOne({ where: { email: value } });
        if (!fetchedUser) {
            throw createErrHandler(404, 'User with such email is not found')
        }
    })
    .trim()
    .escape(),
    (body('password'))
    .notEmpty()
    .withMessage('Password cannot be empty')
    .isLength({ min: 6 })
    .withMessage('Password must be at least 6 characters long')
    .custom(async(value, { req }) => {
        const { email } = req.body
        const fetchedUser = await Users.findOne({ where: { email } });
        const compareResult = comparePass(value, fetchedUser.password);
        if (!compareResult) {
            throw createErrHandler(400, 'Password doesn\'t match')
        }
    })
    .trim()
    .escape()
]

exports.registerValidator = [
  (body('firstName'))
  .notEmpty()
  .withMessage('First Name cannot be empty')
  .trim()
  .escape(),
  (body('lastName'))
  .trim()
  .escape(),
  (body('email'))
  .notEmpty()
  .withMessage('Email cannot be empty')
  .normalizeEmail()
  .isEmail()
  .withMessage('Invalid email format')
  .custom(async value => {
    const fetchedUser = await Users.findOne({where: { email: value }})
    if(fetchedUser){
      throw createErrHandler(400, 'Email already exists!')
    }
  })
  .trim()
  .escape(),
  (body('password'))
  .notEmpty()
  .withMessage('Password cannot be empty')
  .isStrongPassword({ minLength: 8, minLowerCase: 1, minUppercase: 0, minSymbols: 0, minNumbers: 0 })
  .withMessage('Password\'s strength too weak')
  .trim()
  .escape()
]

exports.coursesPostValidator = [
  (body('title'))
  .notEmpty()
  .withMessage('Title cannot be empty')
  .trim()
  .escape(),
  (body('price'))
  .notEmpty()
  .withMessage('Price cannot be empty')
  .trim()
  .escape(),
  (body('region'))
  .notEmpty()
  .withMessage('Region cannot be empty')
  .trim()
  .escape(),
  (body('ingredients'))
  .notEmpty()
  .withMessage('Ingredients cannot be empty')
  .trim()
  .escape(),
  (body('level'))
  .notEmpty()
  .withMessage('Level cannot be empty')
  .trim()
  .escape(),
  (body('duration'))
  .notEmpty()
  .withMessage('Duration cannot be empty')
  .trim()
  .escape(),
]

exports.coursesPutValidator = [
  (body('title'))
  .notEmpty()
  .withMessage('Title cannot be empty')
  .trim()
  .escape(),
  (body('price'))
  .notEmpty()
  .withMessage('Price cannot be empty')
  .trim()
  .escape(),
  (body('region'))
  .notEmpty()
  .withMessage('Region cannot be empty')
  .trim()
  .escape(),
  (body('ingredients'))
  .notEmpty()
  .withMessage('Ingredients cannot be empty')
  .trim()
  .escape(),
  (body('level'))
  .notEmpty()
  .withMessage('Level cannot be empty')
  .trim()
  .escape(),
  (body('duration'))
  .notEmpty()
  .withMessage('Duration cannot be empty')
  .trim()
  .escape(),
]

exports.articlesPostValidator = [
    (body('title'))
    .notEmpty()
    .withMessage('Title cannot be empty')
    .trim()
    .escape(),
    (body('content'))
    .notEmpty()
    .withMessage('Content cannot be empty')
    .trim()
    .escape()
]

exports.articlesPutValidator = [
  (body('title'))
  .notEmpty()
  .withMessage('Title cannot be empty')
  .trim()
  .escape(),
  (body('content'))
  .notEmpty()
  .withMessage('Content cannot be empty')
  .trim()
  .escape()
]

exports.reviewsPostValidator = [
  (body('content'))
  .notEmpty()
  .withMessage('Comment cannot be empty')
  .trim()
  .escape(),
  (body('score'))
  .notEmpty()
  .withMessage('Score cannot be empty')
]

exports.studentPutValidator = [
  (body('firstName'))
  .notEmpty()
  .withMessage('First Name cannot be empty')
  .trim()
  .escape(),
  (body('lastName'))
  .trim()
  .escape(),
  (body('address'))
  .trim()
  .escape()
]

exports.teacherPostValidator = [
  (body('name'))
  .notEmpty()
  .withMessage('Name cannot be empty')
  .custom(async (value) => {
    const fetchedTeacher = await Teachers.findOne({ where: { name: value } })
    if(fetchedTeacher){
      const error = createErrHandler(400, 'Teacher already exists');
      throw error;
    }
  })
  .trim()
  .escape(),
  (body('region'))
  .notEmpty()
  .withMessage('Region cannot be empty')
  .trim()
  .escape()
]

exports.reviewsPutValidator = [
  (body('content'))
  .notEmpty()
  .withMessage('Content cannot be empty')
  .trim()
  .escape(),
  (body('score'))
  .notEmpty()
  .withMessage('Score cannot be empty')
]

exports.teacherPutValidator = [
  (body('name'))
  .notEmpty()
  .withMessage('Name cannot be empty')
  // .custom(async (value) => {
  //   const fetchedTeacher = await Teachers.findOne({ where: { name: value } })
  //   if(fetchedTeacher){
  //     const error = createErrHandler(400, 'Teacher already exists');
  //     throw error;
  //   }
  // })
  .trim()
  .escape(),
  (body('region'))
  .notEmpty()
  .withMessage('Region cannot be empty')
  .trim()
  .escape()
]