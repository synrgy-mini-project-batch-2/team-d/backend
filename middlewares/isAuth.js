const passport = require('passport');
const { asyncErrHandler, createErrHandler } = require('../helpers/errorHelpers');

module.exports = (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if(err){
      return next(err)
    }
    if(!user){
      return next(createErrHandler(401, 'Unauthorized'))
    }
    req.user = user;
    next();
  })(req, res, next);
} 