'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    await queryInterface.removeColumn('Students', 'name');
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.removeColumn('Students', 'firstName');

    await queryInterface.removeColumn('Students', 'lastName');

    await queryInterface.removeColumn('Students', 'imageUrl');

    await queryInterface.addColumn('Students', 'name');
  }
};
