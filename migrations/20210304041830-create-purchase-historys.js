'use strict';
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable('PurchaseHistorys', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            studentId: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'Students',
                    key: 'id',
                    as: 'studentId'
                },
                onDelete: 'SET NULL'
            },
            courseId: {
                allowNull: false,
                type: Sequelize.INTEGER,
                references: {
                    model: 'Courses',
                    key: 'id',
                    as: 'courseId'
                },
                onDelete: 'SET NULL'
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable('PurchaseHistorys');
    }
};