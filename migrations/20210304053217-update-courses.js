'use strict';

const { DataTypes } = require("sequelize");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn(
      'Courses',
      'ingredients',
      {
        type: DataTypes.TEXT
      }
    ),
    await queryInterface.addColumn(
      'Courses',
      'directions',
      {
        type: DataTypes.TEXT
      }
    ),
    await queryInterface.addColumn(
      'Courses',
      'level',
      {
        type: DataTypes.STRING
      }
    ),
    await queryInterface.addColumn(
      'Courses',
      'duration',
      {
        type: DataTypes.STRING
      }
    ),
    await queryInterface.addColumn(
      'Courses',
      'previewUrl',
      {
        type: DataTypes.STRING
      }
    ),
    await queryInterface.removeColumn('Courses', 'content');
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */

     // Ini tidak begitu dipermasalahkan, namun sebaiknya method down ini ada isinya juga. Isinya yaitu kebalikan dari apa yang ada pada method up. Bila di up adalah addColumn, maka disini adalah removeColumn. Ini supaya db:migrate:undo dapat berjalan kalau ada kesalahan.
  }
};
