const express = require('express');
const morgan = require('morgan');
const chalk = require('chalk');
const cors = require('cors');
const passport = require('passport');
const cookieParser = require('cookie-parser');
require('dotenv').config();

const { PORT, NODE_ENV } = process.env;

const routes = require('./routes/api');

const app = express();

const { urlencoded } = require('express');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

if (NODE_ENV === 'development') {
    app.use(morgan('dev'));
}
app.use(cors({ origin: '*', allowedHeaders: 'Content-Type, Authorization' }))

app.use(passport.initialize());
require('./config/passport')(passport);

app.use('/api/v1', routes);


app.listen(PORT || 5000, () => {
    console.log(chalk.blueBright(`Server is running on port ${PORT}`));
})
